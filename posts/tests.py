from django.test import TestCase
from django.contrib.auth.models import User
from .models import Post

class BlogTests(TestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        #Create a user
        test_user = User.objects.create_user(
            username='test', password='test1'
        )
        test_user.save()

        #Create a post
        test_post = Post.objects.create(author=test_user, title='test title', body='test body')
        test_post.save()

    def test_blog_content(self):
        post = Post.objects.get(id=1)
        author = f'{post.author}'
        title = f'{post.title}'
        body = f'{post.body}'

        self.assertEqual(author, 'test')
        self.assertEqual(title, 'test title')
        self.assertEqual(body, 'test body') 
            