# BLOG API 

## Description
Simple web interface, developed with Django, DRF, SQLite. It covered by unit tests and included API documentation (redoc, swagger).

## Installation
```bash
    pip install django, djangorestframework, dj-rest-auth, django-allauth, drf-yasg, pyyaml, uritemplate
```

## Start App
```bash
    python manage.py migrate #for first start
    python manage.py runserver

```
#### Swager and redoc documentations
```bash
    http://localhost:8000/swagger/ 
    http://localhost:8000/redoc/
```

## Tests
```bash
    python manage.py test
```